package com.rapidminer.boyd;

import Jama.Matrix;

import com.rapidminer.tools.math.LinearRegression;

public class DistNode implements Runnable {

	private boolean running = true;
	private FeatureDistributedADMM op;
	private Matrix Ai;
	private Matrix xi;
	private Matrix Axi;
	private CentralNode cn;
	private double lambda;
	private double rho;
	private Matrix zBar;
	private Matrix AxBar;
	private Matrix u;

	public DistNode(Matrix Ai, double lambda, double rho, CentralNode cn,
			FeatureDistributedADMM op) {
		this.Ai = Ai;
		this.lambda = lambda;
		this.rho = rho;
		this.cn = cn;
		this.op = op;
	}

	@Override
	public void run() {
		synchronized (op.startLock) {
			try {
				op.startLock.wait();
			} catch (InterruptedException e) {
			}
		}
		while (running()) {
			calcX();
			synchronized (op.stopLock) {
				op.stopLock.notify();
			}
			synchronized (op.startLock) {
				try {
					op.startLock.wait();
				} catch (InterruptedException e) {
				}
			}
		}
	}

	public void calcX() {
		Matrix b;
		if (xi != null) {
			b = calcB();
		} else {
			b = new Matrix(Ai.getRowDimension(), 1);
		}
		double ridge = 2 * lambda / rho;
		double[] xVec = LinearRegression.performRegression(Ai, b, ridge);
		Matrix x = new Matrix(xVec, xVec.length);
		xi = x;
		Axi = Ai.times(x);
		cn.update(Axi, this);
	}

	private Matrix calcB() {
		Matrix b = Axi.plus(zBar).minus(AxBar).minus(u);
		return b;
	}

	public void update(Matrix zBar, Matrix AxBar, Matrix u, double rho) {
		this.zBar = zBar;
		this.AxBar = AxBar;
		this.u = u;
		this.rho = rho;
	}

	public Matrix getXi() {
		return xi;
	}

	private boolean running() {
		return running;
	}

	public void stop() {
		running = false;
	}
}
