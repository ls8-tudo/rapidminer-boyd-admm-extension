package com.rapidminer.boyd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import Jama.Matrix;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.example.table.DataRow;
import com.rapidminer.example.table.DataRowFactory;
import com.rapidminer.example.table.MemoryExampleTable;
import com.rapidminer.example.table.PolynominalMapping;
import com.rapidminer.operator.Model;
import com.rapidminer.operator.OperatorCapability;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.learner.AbstractLearner;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.metadata.ExampleSetMetaData;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeAttribute;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeDouble;
import com.rapidminer.parameter.ParameterTypeEnumeration;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.conditions.EqualTypeCondition;
import com.rapidminer.tools.Ontology;

public class FeatureDistributedADMM extends AbstractLearner {

	// private final InputPort exampleSetInput = getInputPorts().createPort(
	// "training set", false);
	private final OutputPort distPerfOutput = getOutputPorts().createPort(
			"distribution performance");
	private final OutputPort splittingOutput = getOutputPorts().createPort(
			"splitting");

	private static final String SPLIT_METHOD = "exampleset split method";
	private static final String[] SPLIT_METHODS = { "by order", "random",
			"custom" };
	private static final String SPLIT_EVENLY = "split evenly";
	private static final String CUSTOM_SPLIT = "split list";
	private static final String ATTRIBUTE_LIST = "attribute";
	private static final String LIST_LIST = "list of attributes";
	private static final String NODE_COUNT = "number of nodes";
	private static final String LAMBDA = "lambda";
	private static final String RHO = "rho";
	private static final String EPS_REL = "relative epsilon";
	private static final String EPS_ABS = "absolute epsilon";
	private static final String MU = "mu";
	private static final String TAU_INC = "rho increase factor";
	private static final String TAU_DEC = "rho decrease factor";
	private static final String MAX_ITER = "maximum iterations";
	private static final String THREADED = "run separate threads";

	final Object startLock = new Object();
	final Object stopLock = new Object();
	private MemoryExampleTable splitting;

	public FeatureDistributedADMM(OperatorDescription description) {
		super(description);
		// TODO deliver attributes? maybe create simple exampleset/table with 0
		// examples and clone this after a run?
		distPerfOutput.deliverMD(new ExampleSetMetaData());
		splittingOutput.deliverMD(new ExampleSetMetaData());
	}

	@Override
	public Model learn(ExampleSet exampleSet) throws OperatorException {
		List<Matrix> splitted = new ArrayList<Matrix>();
		int[][] splitIndices = splitExampleSet(exampleSet, splitted);
		int maxIter = getParameterAsInt(MAX_ITER);
		double lambda = getParameterAsDouble(LAMBDA);
		double rho = getParameterAsDouble(RHO);
		double epsRel = getParameterAsDouble(EPS_REL);
		double epsAbs = getParameterAsDouble(EPS_ABS);
		double mu = getParameterAsDouble(MU);
		double tauInc = getParameterAsDouble(TAU_INC);
		double tauDec = getParameterAsDouble(TAU_DEC);
		boolean threaded = getParameterAsBoolean(THREADED);
		CentralNode cn = new CentralNode(maxIter, exampleSet.getAttributes()
				.size() + 1, exampleSet.size(), rho, epsRel, epsAbs, mu,
				tauInc, tauDec, threaded, this);
		List<DistNode> dns = new ArrayList<DistNode>();
		for (Matrix Ai : splitted) {
			dns.add(new DistNode(Ai, lambda, rho, cn, this));
		}
		cn.init(dns);
		// initialize central / dist node
		// start calculation
		cn.run();
		checkForStop();
		double[] x = cn.getX(splitIndices);
		Model m = new ADMMSVMModel(exampleSet, x);
		distPerfOutput.deliver(cn.getHistory());
		splittingOutput.deliver(splitting.createExampleSet());
		return m;
	}

	private int[][] splitExampleSet(ExampleSet exampleSet, List<Matrix> matrices)
			throws OperatorException {
		Matrix A = transformedMatrixFromExamples(exampleSet);
		String splitMethod = getParameterAsString(SPLIT_METHOD);
		int splitType = -1;
		for (int i = 0; i < SPLIT_METHODS.length; i++) {
			if (SPLIT_METHODS[i].equals(splitMethod)) {
				splitType = i;
				break;
			}
		}
		if (splitType < 0) {
			return null;
		}

		int[][] splitIndices;
		Map<String, Integer> attributeMapping = new HashMap<String, Integer>();
		Map<Integer, String> indexMapping = new HashMap<Integer, String>();
		Attributes attributes = exampleSet.getAttributes();
		int i = 0;
		String attName;
		for (Attribute a : attributes) {
			attName = a.getName();
			attributeMapping.put(attName, i);
			indexMapping.put(i++, attName);
		}
		attName = attributes.getLabel().getName();
		attributeMapping.put(attName, i);
		indexMapping.put(i, attName);
		if (splitType == 2) {
			String[] attributeLists = ParameterTypeEnumeration
					.transformString2Enumeration(getParameterAsString(LIST_LIST));
			splitIndices = new int[attributeLists.length][];
			boolean[] used = new boolean[attributeMapping.size()];
			i = 0;
			int j;
			for (String attributeList : attributeLists) {
				String[] attributeNames = ParameterTypeEnumeration
						.transformString2Enumeration(attributeList);
				splitIndices[i] = new int[attributeNames.length];
				j = 0;
				for (String attribute : attributeNames) {
					splitIndices[i][j] = attributeMapping.get(attribute);
					if (used[splitIndices[i][j++]]) {
						throw new OperatorException(
								"Using onw attribute in two nodes is not allowed. Offending attribute: "
										+ attribute);
					}
					used[splitIndices[i][j++]] = true;
				}
				i++;
			}
			List<Integer> unused = new ArrayList<Integer>();
			i = 0;
			for (boolean u : used) {
				if (!u) {
					unused.add(i);
				}
				i++;
			}
			if (!unused.isEmpty()) {
				int[][] tmp = new int[splitIndices.length + 1][];
				int[] rest = new int[unused.size()];
				i = 0;
				for (int u : unused) {
					rest[i++] = u;
				}
				i = 0;
				for (int[] split : splitIndices) {
					tmp[i++] = split;
				}
				tmp[i] = rest;
				splitIndices = tmp;
			}
		} else {
			int nodes = getParameterAsInt(NODE_COUNT);
			int atts = exampleSet.getAttributes().size() + 1;
			splitIndices = new int[nodes][];
			if (splitType == 0
					|| (splitType == 1 && getParameterAsBoolean(SPLIT_EVENLY))) {
				int overfull = atts % nodes;
				int min = atts / nodes;
				int size;
				for (i = 0; i < nodes; i++) {
					size = min + (i < overfull ? 1 : 0);
					splitIndices[i] = new int[size];
				}
			} else {
				Random r = new Random();
				int[] adds = new int[nodes];
				for (i = 0; i < atts - nodes; i++) {
					adds[r.nextInt(nodes)]++;
				}
				for (i = 0; i < nodes; i++) {
					splitIndices[i] = new int[1 + adds[i]];
				}
			}
			if (splitType == 0) {
				int before = 0;
				for (i = 0; i < nodes; i++) {
					for (int j = 0; j < splitIndices[i].length; j++) {
						splitIndices[i][j] = before + j;
					}
					before += splitIndices[i].length;
				}
			} else {
				int[] cap = new int[nodes];
				int[] pos = new int[nodes];
				for (i = 0; i < nodes; i++) {
					cap[i] = splitIndices[i].length;
				}
				int node;
				Random r = new Random();
				for (i = 0; i < atts; i++) {
					do {
						node = r.nextInt(nodes);
					} while (cap[node] == 0);
					splitIndices[node][pos[node]++] = i;
					cap[node]--;
				}
			}
		}
		int m = A.getRowDimension();
		List<Attribute> atts = new ArrayList<Attribute>();
		Attribute att = AttributeFactory.createAttribute("attribute",
				Ontology.NOMINAL);
		att.setMapping(new PolynominalMapping(indexMapping));
		atts.add(att);
		Attribute node = AttributeFactory.createAttribute("node",
				Ontology.NUMERICAL);
		atts.add(node);
		splitting = new MemoryExampleTable(atts, new DataRowFactory(
				DataRowFactory.TYPE_DOUBLE_ARRAY, '.'), indexMapping.size());
		i = 0;
		DataRow row;
		for (int[] columns : splitIndices) {
			matrices.add(A.getMatrix(0, m - 1, columns));
			for (int c : columns) {
				row = splitting.getDataRow(c);
				row.set(att, c);
				row.set(node, i);
			}
			i++;
		}
		return splitIndices;
	}

	private Matrix transformedMatrixFromExamples(ExampleSet exampleSet) {
		Attributes atts = exampleSet.getAttributes();
		int n = atts.size() + 1;
		Matrix A = new Matrix(exampleSet.size(), n);
		int i = 0, j;
		double label;
		for (Example ex : exampleSet) {
			label = ex.getLabel();
			label = 2 * label - 1;
			A.set(i, n - 1, label);
			j = 0;
			for (Attribute a : atts) {
				A.set(i, j++, ex.getValue(a) * label);
			}
			i++;
		}
		return A;
	}

	@Override
	public boolean supportsCapability(OperatorCapability capability) {
		switch (capability) {
		case POLYNOMINAL_LABEL:
		case NUMERICAL_LABEL:
		case ONE_CLASS_LABEL:
		case NO_LABEL:
		case POLYNOMINAL_ATTRIBUTES:
		case BINOMINAL_ATTRIBUTES:
			return false;
		}
		return true;
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> params = super.getParameterTypes();
		ParameterType type = new ParameterTypeCategory(SPLIT_METHOD,
				"Choose the method to split the incoming exampleset.",
				SPLIT_METHODS, 0, false);
		params.add(type);
		type = new ParameterTypeBoolean(SPLIT_EVENLY,
				"Split features evenly across nodes.", true, false);
		type.registerDependencyCondition(new EqualTypeCondition(this,
				SPLIT_METHOD, SPLIT_METHODS, true, 1));
		params.add(type);
		// type = new ParameterTypeString(ATTRIBUTE_LIST,
		// "List of attributes for this node", "", false);
		type = new ParameterTypeAttribute(ATTRIBUTE_LIST,
				"Attribute from exampleset to assign to this node.",
				getExampleSetInputPort());
		type = new ParameterTypeEnumeration(LIST_LIST,
				"The list of attributes assigned to this node.", type);
		// type = new ParameterTypeAttributes(ATTRIBUTE_LIST, "",
		// getExampleSetInputPort());
		type = new ParameterTypeEnumeration(
				CUSTOM_SPLIT,
				"List of attribute lists per node. Attributes that are not named in any node will be collected in a node that is not listed.  Distributing one attribute over more than one node is not allowed.",
				type);
		type.registerDependencyCondition(new EqualTypeCondition(this,
				SPLIT_METHOD, SPLIT_METHODS, true, 2));
		params.add(type);
		type = new ParameterTypeInt(NODE_COUNT,
				"The number of nodes to split the features upon.", 1,
				Integer.MAX_VALUE, 3, false);
		type.registerDependencyCondition(new EqualTypeCondition(this,
				SPLIT_METHOD, SPLIT_METHODS, true, 0, 1));
		params.add(type);
		type = new ParameterTypeDouble(LAMBDA, "The parameter lambda.", 0, 1,
				1e-6, false);
		params.add(type);
		type = new ParameterTypeDouble(RHO, "The parameter rho.", 0,
				Double.POSITIVE_INFINITY, .1, false);
		params.add(type);
		type = new ParameterTypeDouble(EPS_REL,
				"The relative threshold for the stop criteria.", 0, 1, 1e-4,
				false);
		params.add(type);
		type = new ParameterTypeDouble(EPS_ABS,
				"The absolute threshold for the stop criteria.", 0, 1, 1e-2,
				false);
		params.add(type);
		type = new ParameterTypeDouble(MU, "The parameter mu.", 0,
				Double.POSITIVE_INFINITY, 10);
		params.add(type);
		type = new ParameterTypeDouble(TAU_INC,
				"The factor for increasing rho.", 1, Double.POSITIVE_INFINITY,
				2);
		params.add(type);
		type = new ParameterTypeDouble(TAU_DEC,
				"The factor for decreasing rho.", 1, Double.POSITIVE_INFINITY,
				2);
		params.add(type);
		type = new ParameterTypeInt(MAX_ITER, "Maximum number of iterations.",
				1, Integer.MAX_VALUE, 100, false);
		params.add(type);
		type = new ParameterTypeBoolean(THREADED,
				"Run each node in a separat thread.", false, false);
		params.add(type);
		return params;
	}
	// TODO: look at abstractlearner

	// input trainingsset: precondition: collectionpre+simplepre(exaset)

	// output exa: pass through
	// output model: transform rule?
	// output performance/perf-exa: set of transmission values

}
