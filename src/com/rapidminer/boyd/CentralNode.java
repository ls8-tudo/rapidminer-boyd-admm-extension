package com.rapidminer.boyd;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import Jama.Matrix;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.example.table.DataRowFactory;
import com.rapidminer.example.table.ExampleTable;
import com.rapidminer.example.table.MemoryExampleTable;
import com.rapidminer.operator.ProcessStoppedException;
import com.rapidminer.tools.Ontology;

public class CentralNode implements Runnable {

	private int maxIter;
	private FeatureDistributedADMM op;
	private Map<DistNode, Matrix> threadMap;
	private double rho;
	private double epsRel;
	private double epsAbs;
	private double mu;
	private double tauInc;
	private double tauDec;
	private boolean threaded;
	private Matrix zBar;
	private Matrix AxBar;
	private Matrix u;
	private Matrix v;
	private int n;
	private double NInv;
	private double sNorm;
	private double rNorm;
	private int m;
	private List<Double> rhoHistory = new ArrayList<Double>();
	private List<Integer> kHistory = new ArrayList<Integer>();
	private int k;

	public CentralNode(int maxIter, int n, int m, double rho, double epsRel,
			double epsAbs, double mu, double tauInc, double tauDec,
			boolean threaded, FeatureDistributedADMM op) {
		this.maxIter = maxIter;
		this.n = n;
		this.m = m;
		this.rho = rho;
		this.epsRel = epsRel;
		this.epsAbs = epsAbs;
		this.mu = mu;
		this.tauInc = tauInc;
		this.tauDec = tauDec;
		this.threaded = threaded;
		this.op = op;
		kHistory.add(0);
		rhoHistory.add(rho);
	}

	public void init(List<DistNode> threads) {
		this.threadMap = new LinkedHashMap<DistNode, Matrix>();
		for (DistNode dn : threads) {
			threadMap.put(dn, new Matrix(1, 1));
		}
	}

	@Override
	public void run() {
		// start dns
		if (threaded) {
			for (DistNode dn : threadMap.keySet()) {
				new Thread(dn).start();
			}
		}
		int i, N = threadMap.size();
		k = 0;
		NInv = (1.0) / N;
		while (k++ < maxIter) {
			if (threaded) {
				synchronized (op.startLock) {
					op.startLock.notifyAll();
				}
			}
			if (threaded) {
				i = 0;
				while (i++ < N) {
					synchronized (op.stopLock) {
						try {
							op.stopLock.wait();
						} catch (InterruptedException e) {
						}
					}
				}
			} else {
				for (DistNode dn : threadMap.keySet()) {
					dn.calcX();
				}
			}
			zBar = calcZBar();
			u = calcU();
			if (checkStop()) {
				break;
			}
			for (DistNode dn : threadMap.keySet()) {
				dn.update(zBar, AxBar, u, rho);
			}
		}
		kHistory.add(k - 1);
		rhoHistory.add(rho);
		if (threaded) {
			for (DistNode dn : threadMap.keySet()) {
				dn.stop();
			}
			synchronized (op.startLock) {
				op.startLock.notifyAll();
			}
		}
	}

	private Matrix calcZBar() {
		AxBar = calcAxBar();
		v = calcV();
		Matrix oldZ = zBar;
		Matrix zBar = new Matrix(v.getRowDimension(), 1);
		double nOverRho = ((double) n) / rho, vi;
		for (int i = 0; i < zBar.getRowDimension(); i++) {
			vi = v.get(i, 0);
			if (vi > -NInv + nOverRho) {
				vi -= nOverRho;
			} else if (vi >= -NInv) {
				vi = -NInv;
			}
			zBar.set(i, 0, vi);
		}
		sNorm = calcSNorm(oldZ, zBar);
		rNorm = calcRNorm(zBar);
		return zBar;
	}

	private Matrix calcAxBar() {
		Matrix AxBar = null;
		for (Matrix Ax : threadMap.values()) {
			if (AxBar == null) {
				AxBar = (Matrix) Ax.clone();
			}
			AxBar.plusEquals(Ax);
		}
		AxBar.timesEquals(NInv);
		return AxBar;
	}

	private Matrix calcV() {
		Matrix v = AxBar;
		if (u != null) {
			v.plusEquals(u);
		}
		return v;
	}

	private double calcSNorm(Matrix oldZ, Matrix zBar) {
		if (oldZ == null) {
			oldZ = new Matrix(v.getRowDimension(), 1);
		}
		return oldZ.minus(zBar).times(rho).norm2();
	}

	private double calcRNorm(Matrix zBar) {
		return AxBar.minus(zBar).norm2();
	}

	private Matrix calcU() {
		Matrix u = v.minus(zBar);
		adaptRho(u);
		return u;
	}

	private void adaptRho(Matrix u) {
		boolean change = false;
		if (rNorm > mu * sNorm) {
			rho *= tauInc;
			u.timesEquals(1.0 / tauInc);
			change = true;
		} else if (sNorm > mu * rNorm) {
			rho /= tauDec;
			u.timesEquals(tauDec);
			change = true;
		}
		if (change) {
			kHistory.add(k);
			rhoHistory.add(rho);
		}
	}

	public void update(Matrix Axi, DistNode dn) {
		threadMap.put(dn, Axi);
	}

	private boolean checkStop() {
		try {
			op.checkForStop();
		} catch (ProcessStoppedException e) {
			return true;
		}
		double epsPrim = Math.sqrt(n) * epsAbs + epsRel
				* Math.max(AxBar.norm2(), zBar.norm2());
		double epsDual = Math.sqrt(m) * epsAbs + epsRel * rho * u.norm2();
		return rNorm <= epsPrim && sNorm <= epsDual;
	}

	public double[] getX(int[][] splitIndices) {
		double[] x = new double[n];
		Matrix xi;
		int i = 0;
		for (DistNode dn : threadMap.keySet()) {
			xi = dn.getXi();
			for (int j = 0; j < xi.getRowDimension(); j++) {
				x[splitIndices[i][j]] = xi.get(j, 0);
			}
			i++;
		}
		return x;
	}

	public ExampleSet getHistory() {
		List<Attribute> atts = new ArrayList<Attribute>();
		atts.add(AttributeFactory.createAttribute("iteration",
				Ontology.NUMERICAL));
		atts.add(AttributeFactory.createAttribute("rho", Ontology.NUMERICAL));
		ExampleTable distPerf = new MemoryExampleTable(atts,
				new DataRowFactory(DataRowFactory.TYPE_DOUBLE_ARRAY, '.'),
				kHistory.size());
		for (int i = 0; i < kHistory.size(); i++) {
			distPerf.getDataRow(i).set(atts.get(0), kHistory.get(i));
			distPerf.getDataRow(i).set(atts.get(1), rhoHistory.get(i));
		}
		return distPerf.createExampleSet();
	}
}
