package com.rapidminer.boyd;

import java.util.Arrays;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.learner.SimpleBinaryPredictionModel;

public class ADMMSVMModel extends SimpleBinaryPredictionModel {

	private static final long serialVersionUID = -2629543809965776852L;
	private double[] weights;
	private Attributes atts;

	protected ADMMSVMModel(ExampleSet exampleSet, double[] weights) {
		super(exampleSet, 0);
		this.weights = weights;
	}

	@Override
	public ExampleSet apply(ExampleSet exampleSet) throws OperatorException {
		this.atts = exampleSet.getAttributes();
		return super.apply(exampleSet);
	}

	@Override
	public double predict(Example example) throws OperatorException {
		int i = 0;
		int sum = 0;
		for (Attribute a : atts) {
			sum += example.getValue(a) * weights[i++];
		}
		sum += weights[i]; // + or -?
		return -sum <= 0 ? 0 : 1;
	}

	@Override
	public String toString() {
		return Arrays.toString(weights);
	}
}
